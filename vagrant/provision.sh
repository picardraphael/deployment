#!/bin/bash

cd /var/www/deployment/

apt-get update

apt-get install -y curl
apt-get install -y git

# Apache 2

apt-get install -y apache2
a2enmod rewrite ssl actions include headers expires

# MySQL

debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get install -y mysql-server
mysql -u root -p root -e "CREATE DATABASE deployment;"

# PHP 5

apt-get install -y php5-common
apt-get install -y php5-dev
apt-get install -y php5
apt-get install -y php5-curl
apt-get install -y php5-mysql
apt-get install -y php5-cli
apt-get install -y php5-gd
apt-get install -y php5-mcrypt
apt-get install -y mcrypt
apt-get install -y libapache2-mod-php5
apt-get install -y php5-fpm
php5enmod mcrypt

# Apache VHOST

rm -rf /var/www/deployment/log/
mkdir /var/www/deployment/log/
a2dissite 000-default.conf
cp /var/www/deployment/vagrant/apache-vhost.conf /etc/apache2/sites-available/deployment.conf
a2ensite root
service apache2 reload

# Composer

cd /var/www/deployment/
curl -sS https://getcomposer.org/installer | php
php composer.phar install

# DB Migration + Seeds

php artisan migrate
php artisan db:seed

# NPM and Gulp

curl -sL https://deb.nodesource.com/setup_4.x | sh
apt-get update
apt-get install -y nodejs npm
npm install -g gulp
apt-get install -y libnotify-bin

# NPM Packages & gulp tasks

npm install
gulp

# The end

echo "Don't forget to make some folders writable"
echo "chmod -R 777 storage"
echo "chmod -R 777 bootstrap/cache"

sudo service apache2 restart
