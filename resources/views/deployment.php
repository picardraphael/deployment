<?php

//Setup by script
$GLOBAL_STAGING_DIR='/var/www/deployment';
$GLOBAL_STAGING_BRANCH='develop';
$GLOBAL_PROD_DIR='/var/www/deployment-prod';
$GLOBAL_PROD_BRANCH='master';
// choices : laravel, wordpress, other
$GLOBAL_TYPE='laravel';

//Script is on staging
$is_staging = true;

//test if the shell_exec function is enabled on this server
if(!function_exists('shell_exec')) {
    echo "shell_exec is disabled";
    die();
}

//Set dir and branch for staging and prod
$staging_dir = $GLOBAL_STAGING_DIR;
$staging_branch = $GLOBAL_STAGING_BRANCH;
$prod_dir = $GLOBAL_PROD_DIR;
$prod_branch = $GLOBAL_PROD_BRANCH;

//set the default directory and branch
$use_dir = $staging_dir;
$use_branch = $staging_branch;

//if update, default don't update
$update = false;

if (!$is_staging){
    $update =	true;
    $use_dir = $prod_dir;
    $use_branch = $staging_branch;
}

echo " Site is deploy on : branche -> $use_branch / dir : $use_dir";
shell_exec('cd ' . $use_dir . ' && git reset --hard origin/'. $use_branch. ' && git clean -f && git pull && git checkout -f '.$use_branch.' 2>&1');

