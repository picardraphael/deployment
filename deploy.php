<?php
/**
 * Auto deploy script bitbucket
 * Please, read the read_me.md
 * to call this script on php :
 * php /path/to/script/deploy.php
 * © 1Agence 2017
 */

/**
 * Put your config here
 */
// Path to project
$DEV_DIR='/var/www/deployment';
// Use branche (staging)
$DEV_BRANCH='develop';
// Put your additional command here (staging)
$DEV_COMMANDS=array(
    "php artisan migrate"
);
// Path to project (prod)
$PROD_DIR='/var/www/deployment-master';
// Use branche (prod)
$PROD_BRANCH='master';
// Put your additional command here (prod)
$PROD_COMMANDS=array();

// deploy
$deploy = false;
// display output
$display_output = true;

$use_dir=$DEV_DIR;
$use_branch=$DEV_BRANCH;
$use_commands=$DEV_COMMANDS;

/**
 * Display help
 */
function displayHelp(){
    echo "Script deployment for 1Agence : \r\n";
    echo "-h : Display help\r\n";
    echo "-o : Don't display output (default is display)\r\n";
    echo "-d : Deploy staging\r\n";
    echo "-p : Deploy prod\r\n";
    echo "Example :\r\n";
    echo "php /path/to/script/deploy.php -d -o\r\n";
    echo "Or :\r\n";
    echo "exec(\"php /path/to/script/deploy.php -p\");\r\n\r\n";
    echo "For more detail read doc on : http://deployment.1agence.com\r\n";
}

// Get all arg
foreach ($argv as $item) {
    switch ($item) {
        case "-h" :
            displayHelp();
            die();
            break;
        case "-o" :
            $display_output = false;
            break;
        case "-d" :
            $use_dir=$DEV_DIR;
            $use_branch=$DEV_BRANCH;
            $use_commands=$DEV_COMMANDS;
            $deploy = true;
            break;
        case "-p" :
            $use_dir=$PROD_DIR;
            $use_branch=$PROD_BRANCH;
            $use_commands=$PROD_COMMANDS;
            $deploy = true;
            break;
    }
}

if (!$deploy){
    echo "Error deployment, run php deploy.php -h for more informations\r\n";
    die();
}

// Your commands
$commands = array(
    'cd '.$use_dir,
    'git reset --hard origin/'.$use_branch,
    'git clean -f',
    'git pull',
    'git checkout '.$use_branch
);

// Merge default command and custom command
$commands = array_merge($commands, $use_commands);

// All output result
$output = '';

// Get all commands
foreach($commands AS $command){
    // Run it
    $tmp = shell_exec($command);
    // Set output
    $output .= "$command\r\n";
    $output .= htmlentities(trim($tmp)) . "\r\n";
}

// Display output
if ($display_output){
    echo $output;
}

?>