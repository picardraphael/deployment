<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnvironmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('environments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('website_id')->unsigned()->nullable();
            $table->string('type', 100);
            $table->string('host', 100);
            $table->string('username', 100);
            $table->string('password', 100);
            $table->string('keyphrase', 255);
            $table->string('agent', 100);
            $table->integer('timeout')->default(10);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('environments', function(Blueprint $table)
        {
            $table->foreign('website_id')->references('id')->on('websites');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('environments');
    }
}
