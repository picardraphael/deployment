<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Command extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'commands';

    protected $fillable = [
        'id', 'environment_id', 'line', 'order'
    ];

    protected $hidden = [];

    public function environment()
    {
        return $this->belongsTo('App\Environment', 'environment_id', 'id');
    }
}
