<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Environment extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'environments';

    protected $fillable = [
        'id', 'website_id', 'type', 'host', 'username', 'password', 'keyphrase', 'agent', 'timeout'
    ];

    protected $hidden = [];

    public function website()
    {
        return $this->belongsTo('App\Website', 'website_id', 'id');
    }

    public function commands()
    {
        return $this->hasMany('App\Command', 'environment_id', 'id')->orderBy("order", "asc");
    }

}
