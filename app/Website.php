<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Website extends Authenticatable
{
    use SoftDeletes;

    protected $table = 'websites';

    protected $fillable = [
        'id', 'name', 'friendly_name'
    ];

    protected $hidden = [];

    public function environments()
    {
        return $this->hasMany('App\Environment', 'website_id', 'id');
    }

}
