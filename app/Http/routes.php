<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/ssh/{id?}', "SshController@index");

/**
 * Api routes
 */
Route::group(['prefix' => 'api/v1', 'namespace' => 'Api\V1'], function ($router) {

    /**
     * Deployment route
     */
    Route::group(['prefix' => 'deployment', 'namespace' => 'Deployment'], function ($router) {

        /**
         * Websites
         */
        $router->post('/websites', 'WebsiteController@create');
        $router->put('/websites/{website_id}', 'WebsiteController@update');
        $router->delete('/websites/{website_id}', 'WebsiteController@delete');
        $router->get('/websites', 'WebsiteController@getAll');
        $router->get('/websites/{website_id}', 'WebsiteController@getById');

        /**
         * Environments
         */
        $router->post('/websites/{website_id}/environments', 'EnvironmentController@create');
        $router->put('/websites/{website_id}/environments/{environment_id}', 'EnvironmentController@update');
        $router->delete('/websites/{website_id}/environments/{environment_id}', 'EnvironmentController@delete');
        $router->get('/websites/{website_id}/environments/', 'EnvironmentController@getAll');
        $router->get('/websites/{website_id}/environments/{environment_id}', 'EnvironmentController@getById');

        /**
         * Commands
         */
        $router->post('/websites/{website_id}/environments/{environment_id}/commands', 'CommandController@create');
        $router->put('/websites/{website_id}/environments/{environment_id}/commands/{command_id}', 'CommandController@update');
        $router->delete('/websites/{website_id}/environments/{environment_id}/commands/{command_id}', 'CommandController@delete');
        $router->get('/websites/{website_id}/environments/{environment_id}/commands/', 'CommandController@getAll');
        $router->get('/websites/{website_id}/environments/{environment_id}/commands/{command_id}', 'CommandController@getById');

        /**
         * Deploy script (Call by bitbucket webhook)
         */
        $router->post('/run/{website_id}', 'DeploymentController@deploy');
        $router->post('/run/{website_id}/force/{env}', 'DeploymentController@forceDeploy');
    });
});

