<?php

namespace App\Http\Controllers\Api\V1\Deployment;

use App\Website;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function create(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:websites,name',
            'friendly_name' => 'required'
        ]);

        try {

            $website = new Website();
            $website->fill($request->all());
            $website->save();

            return response()->json($website);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function update($website_id, Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:websites,name,' . $website_id,
            'friendly_name' => 'required'
        ]);

        try {

            $website = Website::find($website_id);
            $website->fill($request->all());
            $website->save();

            return response()->json($website);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function delete($website_id)
    {
        try {

            $website = Website::find($website_id);
            $website->delete();

            return response()->json(["status" => "deleted"]);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getAll()
    {
        try {

            $website = Website::orderBy("id", "asc")
                ->get();

            return response()->json($website);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getById($website_id)
    {
        try {
            $website = Website::find($website_id);

            return response()->json($website);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
