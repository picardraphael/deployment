<?php

namespace App\Http\Controllers\Api\V1\Deployment;

use App\Environment;
use App\Website;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Log\Writer;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Application;
use Carbon\Carbon;
use SSH;

class DeploymentController extends Controller
{
    private $output = "\r\n";

    public function deploy($website_id, Request $request, Writer $log, Application $app)
    {
        try {
            $environment = Environment::where("id", $website_id)
                ->where('type', 'develop')
                ->with("commands")
                ->with("website")
                ->first();

            return $this->run($environment, $log, $app);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function forceDeploy($website_id, $env, Request $request, Writer $log, Application $app)
    {
        try {
            $environment = Environment::where('type', $env)
                ->where('type', (string) $env)
                ->with("commands")
                ->with("website")
                ->first();
            
            return $this->run($environment, $log, $app);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    private function run($environment, $log, $app)
    {
        try {
            $currentDate = Carbon::now();

            $log->useFiles($app->storagePath() . '/logs/' . $currentDate->toDateString() . '-' . $environment->website->id . '-' . $environment->website->name . '.log');

            $log->info("Deployment start on [" . $environment->website->name . " => " . $environment->type . "] at " . $currentDate);

            Config::set('remote.connections.runtime.host', $environment->host);
            Config::set('remote.connections.runtime.username', $environment->username);
            Config::set('remote.connections.runtime.password', $environment->password);
            Config::set('remote.connections.runtime.keyphrase', $environment->keyphrase);
            Config::set('remote.connections.runtime.agent', $environment->agent);
            Config::set('remote.connections.runtime.timeout', $environment->timeout);

            $commands = array();

            foreach ($environment->commands as $command) {
                $commands[] = $command->line;
            }

            SSH::into("runtime")->run($commands, function ($line) {
                $this->output .= $line;
            });

            $log->info($this->output);
            $log->info("Deployment finish on [" . $environment->website->name . " => " . $environment->type . "] at " . Carbon::now());
            $log->info("======================");

            return response()->json(["deployment" => "success"]);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
