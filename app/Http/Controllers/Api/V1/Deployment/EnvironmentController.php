<?php

namespace App\Http\Controllers\Api\V1\Deployment;

use App\Environment;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class EnvironmentController extends Controller
{
    public function create($website_id, Request $request)
    {

        $this->validate($request, [
            'website_id' => 'required',
            'type' => 'required',
            'host' => 'required'
        ]);

        try {

            $environment = new Environment();
            $environment->fill($request->all());
            $environment->website_id = $website_id;
            $environment->save();

            return response()->json($environment);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function update($website_id, $environment_id, Request $request)
    {

        $this->validate($request, [
            'website_id' => 'required',
            'type' => 'required',
            'host' => 'required'
        ]);

        try {

            $environment = Environment::find($environment_id);
            $environment->fill($request->all());
            $environment->website_id = $website_id;
            $environment->save();

            return response()->json($environment);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function delete($website_id, $environment_id)
    {
        try {

            $environment = Environment::find($environment_id);
            $environment->delete();

            return response()->json(["status" => "deleted"]);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getAll($website_id)
    {
        try {

            $environment = Environment::orderBy("id", "asc")
                ->where("website_id", $website_id)
                ->get();

            return response()->json($environment);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getById($website_id, $environment_id)
    {
        try {
            $environment = Environment::find($environment_id);

            return response()->json($environment);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
