<?php

namespace App\Http\Controllers\Api\V1\Deployment;

use App\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CommandController extends Controller
{
    public function create($website_id, $environment_id, Request $request)
    {

        $this->validate($request, [
            'environment_id' => 'required',
            'line' => 'required'
        ]);

        try {

            $environment = new Command();
            $environment->fill($request->all());
            $environment->environment_id = $environment_id;
            $environment->save();

            return response()->json($environment);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function update($website_id, $environment_id, $command_id, Request $request)
    {

        $this->validate($request, [
            'environment_id' => 'required',
            'line' => 'required'
        ]);

        try {

            $environment = Command::find($command_id);
            $environment->fill($request->all());
            $environment->environment_id = $environment_id;
            $environment->save();

            return response()->json($environment);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function delete($website_id, $environment_id, $command_id)
    {
        try {

            $environment = Command::find($command_id);
            $environment->delete();

            return response()->json(["status" => "deleted"]);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getAll($website_id, $environment_id)
    {
        try {

            $environment = Command::orderBy("id", "asc")
                ->where("environment_id", $environment_id)
                ->get();

            return response()->json($environment);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getById($website_id, $environment_id, $command_id)
    {
        try {
            $environment = Command::where("id", $command_id)
                ->where("environment_id", $environment_id)
                ->first();

            return response()->json($environment);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'not_found'], 404);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
