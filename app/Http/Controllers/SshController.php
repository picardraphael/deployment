<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Collective\Remote\RemoteFacade;
use Illuminate\Log\Writer;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use SSH;

class SshController extends Controller
{
    private $output = "\r\n";

    public function index($id = null, Request $request, Writer $log, Application $app)
    {
        $currentDate = Carbon::now();

        $log->useFiles($app->storagePath().'/logs/'.$currentDate->toDateString().'-'.$id.'-Command.log');

        $log->info("Deployment start on [friendly_name] at ".$currentDate);

        Config::set('remote.connections.runtime.host', '198.50.242.248');
        Config::set('remote.connections.runtime.username', 'ubuntu');
        Config::set('remote.connections.runtime.password', '!!1Agence2017');
        Config::set('remote.connections.runtime.timeout', 10);

        SSH::into("runtime")->run([
            'cd /var/www',
            'ls -la',
        ], function($line)
        {
            $this->output .= $line;
        });

        $log->info($currentDate->toDateString());
        $log->info("Deployment finish on [friendly_name] at ".Carbon::now());
        $log->info("======================");
    }



}