<?php

//Setup by script
$GLOBAL_STAGING_DIR='';
$GLOBAL_STAGING_BRANCH='';
$GLOBAL_PROD_DIR='';
$GLOBAL_PROD_BRANCH='';
// choices : laravel, wordpress, other
$GLOBAL_TYPE='wordpress';

//test if the shell_exec function is enabled on this server
if(!function_exists('shell_exec')) {
    echo "shell_exec is disabled";
    die();
}

//Set dir and branch for staging and prod
$staging_dir = $GLOBAL_STAGING_DIR;
$staging_branch = $GLOBAL_STAGING_BRANCH;
$prod_dir = $GLOBAL_PROD_DIR;
$prod_branch = $GLOBAL_PROD_BRANCH;

//set the default directory and branch
$use_dir = $staging_dir;
$use_branch = $staging_branch;

//if update, default don't update
$update = false;
//get Bitbucket post data
$payload = json_decode($_POST['payload']);

if (empty($payload->commits)){
    $update = true;
} else {
    foreach ($payload->commits as $commit) {
        $branch = $commit->branch;
        if ($branch === $prod_branch || isset($commit->branches) && in_array($prod_branch, $commit->branches)) {
            $update =	true;
            $use_dir = $prod_dir;
            $use_branch = $staging_branch;
            break;
        }

        if ($branch === $staging_branch || isset($commit->branches) && in_array($staging_branch, $commit->branches)) {
            $update =	true;
            $use_dir = $staging_dir;
            $use_branch = $staging_branch;
            break;
        }
    }
}

//Update site
if ($update && isset($_POST)){
    shell_exec('cd ' . $use_dir . ' && git reset --hard origin/'. $use_branch. ' && git clean -f && git pull && git checkout '.$use_branch.' 2>&1');
}

/**
 * ADD your custom command
 * LARAVEL
 */
if ($GLOBAL_TYPE == 'laravel') {
    //shell_exec('cd ' . $use_dir . ' && YOUR_COMMANDE 2>&1');
}

/**
 * ADD your custom command
 * WORDPRESS
 */
if ($GLOBAL_TYPE == 'wordpress') {
    //shell_exec('cd ' . $use_dir . ' && YOUR_COMMANDE 2>&1');
}

/**
 * ADD your custom command
 * OTHER
 */
if ($GLOBAL_TYPE == 'other') {
    //shell_exec('cd ' . $use_dir . ' && YOUR_COMMANDE 2>&1');
}

